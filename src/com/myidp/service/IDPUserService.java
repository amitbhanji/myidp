package com.myidp.service;

import com.myidp.dao.IDPUserDAO;
import com.myidp.dao.MySQLIDPUserDAO;
import com.myidp.dao.OracleAccountDAO;
import com.myidp.vo.IDPUser;

/**
 * Service layer for Account
 */
public class IDPUserService {
	
	public enum DatabaseType {ORACLE, MYSQL;
		public IDPUserDAO getAccountDAO(){
			switch(this){
				case ORACLE: return new OracleAccountDAO();
				case MYSQL: return new MySQLIDPUserDAO();
				default:
					return null;
			}
		}
	
	};
	IDPUserDAO IDPUserDAO;
	
	/**
	 * Get an instance of the IDPUserService
	 * @param databaseType type of the database
	 */
	public IDPUserService(DatabaseType databaseType){
		IDPUserDAO = databaseType.getAccountDAO();
	}

	/**
	 * Create a new user
	 * 
	 * @param user
	 *            user to be created
	 * @return created user
	 */
	public IDPUser createUser(IDPUser user) {
		return IDPUserDAO.createUser(user);
	}


	/**
	 * Retrieve an account
	 * 
	 * @param email
	 *            identifier of the account to be retrieved
	 * @return account represented by the identifier provided
	 */
	public IDPUser getUser(String email) {
		return IDPUserDAO.getUser(email);
	}
}
