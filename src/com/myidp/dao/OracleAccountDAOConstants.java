package com.myidp.dao;

public class OracleAccountDAOConstants {

	public static final String URL = "jdbc:oracle:thin:lecture1/lecture1@localhost:1521:XE";

	public static String GET_ACCOUNT_ID = "SELECT * FROM IDPUSER WHERE EMAIL = ?";

	public static String CREATE_IDPUSER = "INSERT INTO ACCOUNT(FIRST_NAME, LAST_NAME, EMAIL, PASSWORD) VALUES(?,?,?,?)";

}
