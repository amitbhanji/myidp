package com.myidp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.myidp.vo.IDPUser;


/**
 * Data Access Object for Account
 */
public class OracleAccountDAO extends IDPUserDAO{

	/**
	 * @see IDPUserDAO.hibernate.dao.AccountDAO#createAccount()
	 */
	public IDPUser createUser(IDPUser idpUser) {
		Connection connection = null;
		PreparedStatement createUserStatement = null;
		ResultSet resultSet = null;																									
		try {
			connection = getConnection();

			createUserStatement = connection.prepareStatement(OracleAccountDAOConstants.CREATE_IDPUSER);
			createUserStatement.setString(1, idpUser.getFirstName());
			createUserStatement.setString(1, idpUser.getLastName());
			createUserStatement.setString(2, idpUser.getEmail());
			createUserStatement.setString(3, idpUser.getPassword());
			createUserStatement.execute();

			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try{
				connection.rollback();
			}catch(SQLException e1){
				throw new RuntimeException(e1);
			}
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			cleanupDatabaseResources(null, createUserStatement, resultSet);
			cleanupDatabaseResources(connection, createUserStatement, null);
		}
		return getUser(idpUser.getEmail());
	}

	/**
	 * @see IDPUserDAO.hibernate.dao.AccountDAO#getConnection()
	 */
	protected Connection getConnection() throws SQLException {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		Connection connection = DriverManager.getConnection(OracleAccountDAOConstants.URL);
		connection.setAutoCommit(false);
		return connection;
	}

}
