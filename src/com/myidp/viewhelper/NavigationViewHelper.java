package com.myidp.viewhelper;

import javax.servlet.http.HttpServletRequest;

public class NavigationViewHelper implements GenericViewHelper{
	@Override
	public String execute(HttpServletRequest request, String uiActionName) throws Exception {
		System.out.println("processing navigation::");
		return uiActionName;
	}

}
