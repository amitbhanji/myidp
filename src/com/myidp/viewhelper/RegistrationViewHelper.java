package com.myidp.viewhelper;

import javax.servlet.http.HttpServletRequest;

import com.myidp.service.IDPUserService;
import com.myidp.service.IDPUserService.DatabaseType;
import com.myidp.vo.IDPUser;

public class RegistrationViewHelper implements GenericViewHelper{
	@Override
	public String execute(HttpServletRequest request, String uiActionName) throws Exception {
		System.out.println("processing registration::");
		
		return createUser(request, uiActionName);
	}

	private String createUser(HttpServletRequest request, String uiActionName) {
		
		IDPUser idpUser = new IDPUser();
		if(request.getParameter("firstName") != null) {
			idpUser.setFirstName(request.getParameter("firstName"));
		}
		if(request.getParameter("lastName") != null) {
			idpUser.setLastName(request.getParameter("lastName"));
		}
		if(request.getParameter("email") != null) {
			idpUser.setEmail(request.getParameter("email"));
		}
		if(request.getParameter("password") != null) {
			idpUser.setPassword(request.getParameter("password"));
		}

		IDPUserService idpUserService = new IDPUserService(DatabaseType.MYSQL);
		idpUserService.createUser(idpUser);
		return uiActionName;
	}

}
