package com.myidp.dao;

public class IDPUserDAOConstants {

	public static String UPDATE_ACCOUNT = "UPDATE ACCOUNT SET BALANCE = ? WHERE ACCOUNT_ID = ?";

	public static String DELETE_ACCOUNT = "DELETE FROM ACCOUNT WHERE ACCOUNT_ID = ?";

	public static String GET_IDPUSER = "SELECT * FROM IDPUSER WHERE EMAIL = ?";

}
