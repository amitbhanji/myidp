package com.myidp.dao;

public class MySQLIDPUserDAOConstants {

	public static final String URL = "jdbc:mysql://root:admin@localhost:3306/myidp";

	public static String GET_ACCOUNT_ID = "SELECT * FROM IDPUSER WHERE EMAIL = ?";

	public static String CREATE_IDPUSER = "INSERT INTO IDPUser(FIRST_NAME, LAST_NAME, EMAIL, PASSWORD) VALUES(?,?,?,?)";
}
