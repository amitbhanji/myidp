package com.myidp.viewhelper;

import javax.servlet.http.HttpServletRequest;

import com.myidp.service.IDPUserService;
import com.myidp.service.IDPUserService.DatabaseType;
import com.myidp.vo.IDPUser;

public class LoginViewHelper implements GenericViewHelper{
	@Override
	public String execute(HttpServletRequest request, String uiActionName) throws Exception {
		System.out.println("processing registration::");
	
		if(uiActionName.equals("doValidLogin")) {
			String loginMsg = "";
			IDPUser loggedInUser = new IDPUser();
			if(request.getParameter("email") != null) {
				loggedInUser.setEmail(request.getParameter("email"));
			}
			if(request.getParameter("password") != null) {
				loggedInUser.setPassword(request.getParameter("password"));
			}
			IDPUserService idpUserService = new IDPUserService(DatabaseType.MYSQL);
			IDPUser idpUser = idpUserService.getUser(loggedInUser.getEmail());
			if(idpUser.getEmail().equals(loggedInUser.getEmail()) && 
					idpUser.getPassword().equals(loggedInUser.getPassword()))
			{
				loginMsg = "Hi, "+loggedInUser.getEmail()+ ". Welcome to Dashboard";
			}
			else
			{
				loginMsg = "Incorrect User Email or Password";
			}
			request.setAttribute("loginmsg", loginMsg);
		}
		return uiActionName;
	}

}
