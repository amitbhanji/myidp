package com.myidp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.myidp.vo.IDPUser;


/**
 * Data Access Object for Account
 */
public abstract class IDPUserDAO {

	/**
	 * Create a new account
	 * 
	 * @param idpUser
	 *            account to be created
	 * @return created account
	 */
	public abstract IDPUser createUser(IDPUser idpUser);

	/**
	 * Get a connection to the data store
	 * @return database connection
	 * @throws SQLException if an error occurs getting a connection
	 */
	protected abstract Connection getConnection() throws SQLException;

	/**
	 * Retrieve an user from the data store
	 * 
	 * @param email
	 *            identifier of the account to be retrieved
	 * @return account represented by the identifier provided
	 */
	public IDPUser getUser(String email) {
		Connection connection = null;
		PreparedStatement getAccountStatement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			getAccountStatement = connection.prepareStatement(IDPUserDAOConstants.GET_IDPUSER);
			getAccountStatement.setString(1, "'"+email+"'");
			resultSet = getAccountStatement.executeQuery();

			IDPUser idpUser = null;
			if (resultSet.next()) {
				idpUser = new IDPUser();
				idpUser.setFirstName(resultSet.getString("FIRST_NAME"));
				idpUser.setLastName(resultSet.getString("LAST_NAME"));
				idpUser.setEmail(resultSet.getString("EMAIL"));
				idpUser.setPassword(resultSet.getString("PASSWORD"));
			}
			connection.commit();
			return idpUser;

		} catch (SQLException e) {
			e.printStackTrace();
			try{
				connection.rollback();
			}catch(SQLException e1){
				throw new RuntimeException(e1);
			}
			throw new RuntimeException(e);
		} finally {
			cleanupDatabaseResources(connection, getAccountStatement, resultSet);
		}
	}

	/**
	 * Clean up database resources 
	 * @param connection connection to close
	 * @param statement statement to close
	 * @param resultSet resultSet to close
	 */
	protected void cleanupDatabaseResources(Connection connection,
			Statement statement, ResultSet resultSet) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


}
